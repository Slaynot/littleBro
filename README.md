# LittleBro

LittleBro is a monkey in the middle python script based on scapy.

At the moment, it can:

- MITM with ARP poisoning
- Log network trafic in a pcap file
- Change all DNS answers with an ip provided by the user

LittleBro do the routing process by himself, and ATM, it's pretty slow. It can handle classic surf trafic, but if the target starts downloading, littelBro will have trouble keeping up.

But hey, it's just a fun project primarly made for toying on a virtual LAN. Go download Ettercap if you want the big guns ! ;)

## Dependencies

- python 2.7
- scapy


## Playing with littleBro

python littleBro.py -h

=P

## Author

Slaynot <slaynot@zaclys.net>
