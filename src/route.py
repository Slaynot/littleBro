# -*- coding: utf-8 -*-

import threading, Queue
import networkTools, dnsFactory, parameters
from scapy.all import DNSRR, UDP, Ether, TCP, sendp, wrpcap


class Route(threading.Thread):
    """
    Transmit packets one way
    """

    def __init__(self, source_mac, target_mac):

        threading.Thread.__init__(self)

        self.source_mac = source_mac
        self.target_mac = target_mac

        self.packets = Queue.Queue(maxsize=0)

        self.terminated = False



    def run(self):

        # Sending packet endlessly
        while True:
            packet = self.packets.get()
            self.transmit(packet)

    def transmit(self, packet):
        """
        Transmit packet to the target
        """

        packet.dst = self.target_mac
        # If DNS, rebuild it to avoid corruption
        if packet.haslayer(DNSRR):
            if packet.haslayer(UDP):
                packet = dnsFactory.build_UDP(packet)
                del packet[Ether].chksum
            elif packet.haslayer(TCP):
                packet = dnsFactory.build_TCP(packet)
                del packet[Ether].chksum

        if parameters.args.output != None:
            self.writePcap(packet)

        packet.src = networkTools.get_own_mac(parameters.args.interface)

        try:
            sendp(packet, verbose=0, iface=parameters.args.interface)
        except Exception, e:
            print str(e)
        if parameters.args.verbose:
            parameters.print_verbose("Routing packet to " + self.target_mac)

    def writePcap(self, packet):
        """
        Write in .pcap file
        """

        if parameters.args.verbose:
            parameters.print_verbose("Logging packet")
        wrpcap(parameters.args.output + ".pcap", packet, append=True)

    def add_packet(self, paquet):
        """
        Add packet to route
        """

        self.packets.put(paquet)
