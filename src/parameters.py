# -*- coding: utf-8 -*-

#########################################################################
# LittleBro, an open source monkey in the middle python script          #
#                                                                       #
# Author : Slaynot <slaynot@zaclys.net>                                 #
#                                                                       #
# LittleBro is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# A full copy of the GNU General Public License can be found in LICENCE #
#########################################################################

import argparse, os, time, sys, networkTools

parser = argparse.ArgumentParser()
parser.add_argument("target_1", help="First IP address to target")
parser.add_argument("target_2", help="Second IP address to target")
parser.add_argument("interface", help="The interface littleBro will be using")
parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
parser.add_argument("-p", "--password", help="catch HTTP login/password (not implemented yet !)", action="store_true")
parser.add_argument("-o", "--output", type=str, help="Pcap's file name")
parser.add_argument("-d", "--dns_spoofing", type=str, help="The IP to insert in DNS answers")
parser.add_argument("--process", type=int, default=1, help="Number of threads used for routing (kinda useless)")
args = parser.parse_args()

start = time.time()

if args.output != None:
    # Is there a pcap file ? 
    try:
        with open(args.output + ".pcap"): pass
        # Yup, get rid of it !
        os.remove(args.output + ".pcap")
    except IOError:
        pass

if args.dns_spoofing != None:
    if not networkTools.is_valid_ipv4_address(args.dns_spoofing):
        print "option -d: invalid IP, exiting"
        sys.exit()

def get_time():
    return str(round(time.time() - start, 1))

def print_verbose(texte):
    print get_time() + " - " + texte
