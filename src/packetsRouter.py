# -*- coding: utf-8 -*-

#########################################################################
# LittleBro, an open source monkey in the middle python script          #
#                                                                       #
# Author : Slaynot <slaynot@zaclys.net>                                 #
#                                                                       #
# LittleBro is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# A full copy of the GNU General Public License can be found in LICENCE #
#########################################################################

import threading
import networkTools, parameters
from scapy.all import IP, sniff

class PacketsRouter(threading.Thread):
    """
    Route packets and do all the changes to play in the trafic !
    """

    def __init__(self, way1, way2, mac1, mac2):

        threading.Thread.__init__(self)

        self.way1 = way1
        self.way2 = way2

        self.numThread = len(way1)

        self.num1 = 0
        self.num2 = 0

        self.mac1 = mac1
        self.mac2 = mac2

    # Executé au lancement du thread
    def run(self):
        """
        Launch the two way routing, sniff packets and feed routes
        """
        for i in range(self.numThread):
            self.way1[i].start()
            self.way2[i].start()
        
        # Packet sniffing !
        while True:
            packets = sniff(prn=self.packetRouting, iface=parameters.args.interface, filter="ip")

    def packetRouting(self, packets):
        """
        Do the routing magic
        """

        for packet in packets:
            if packet.src == self.mac1 and packet[IP].dst != networkTools.get_own_ip(parameters.args.interface):
                if parameters.args.verbose:
                    parameters.print_verbose("Packet received: " + packet.summary() + " from " + packet.src)
                self.way1[self.num1].add_packet(packet)
                if self.num1 + 1 < self.numThread:
                    self.num1 += 1
                else:
                    self.num1 = 0
            elif packet.src == self.mac2 and packet[IP].dst != networkTools.get_own_ip(parameters.args.interface):
                if parameters.args.verbose:
                    parameters.print_verbose("Packet received:  " + packet.summary() + "  from " + packet.src)
                self.way2[self.num2].add_packet(packet)
                if self.num2 + 1 < self.numThread:
                    self.num2 += 1
                else:
                    self.num2 = 0
