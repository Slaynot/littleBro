# -*- coding: utf-8 -*-

#########################################################################
# LittleBro, an open source monkey in the middle python script          #
#                                                                       #
# Author : Slaynot <slaynot@zaclys.net>                                 #
#                                                                       #
# LittleBro is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# A full copy of the GNU General Public License can be found in LICENCE #
#########################################################################

import fcntl, socket, struct
from scapy.all import Ether, ARP, srp

def get_own_mac(ifname):
    """
    Return current interface's MAC address
    """

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', ifname[:15]))
    return ':'.join(['%02x' % ord(char) for char in info[18:24]])

def get_own_ip(ifname):
    """
    Return current interface's IP address
    """

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])

def get_mac_address(IPcible, IPspoof, interface):
    """
    Return the target's MAC address, meanwhile spoofing another machine's IP address
    """

    arp = Ether(dst="ff:ff:ff:ff:ff:ff") / ARP(op=1, pdst=IPcible, psrc=IPspoof)
    rep, no_rep = srp(arp, iface=interface, verbose=0)
    original_packet, answer = rep[0]
    return answer.src

def is_valid_ipv4_address(address):
    try:
        socket.inet_aton(address)
        return True
    except socket.error, e:
        print str(e)
        return False
